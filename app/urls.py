from django.urls import path

from .views import *


urlpatterns = [
    path('',ModeloView.as_view(),name="modelo_list"),   
    path('new',ModeloNew.as_view(),name="new"),
    path('edit/<int:pk>',ModeloEdit.as_view(),name="edit"), 
    path('crud',Crud.as_view(),name="crud"),
    path('crud/update',update1,name="update1"),
    path('crud/reload',reload,name="reload"),
    path('delete/<int:pk>',ModeloDel.as_view(),name="delete"), 

    path('crud/update2',update2,name="update2"),

    path('bootstable',BootsTable.as_view(),name="bootstable"),

    path('datatable',DataTable.as_view(),name="datatable"),
    path('datatable/crud',datatable,name="datatable2"),

    path('datatable-custom',DataTableCustom.as_view(),name='datatable_custom'),

    path('bootstrap-table',BoostrapTable.as_view(),name="bootstrap_table"),
    path('crud/reload2',reload2,name="reload2"),
    path('bootstrap-table-server-side',BootstrapTableServerSide.as_view(),name="bootstrap_table_server_side"),
    path('crud/bt-serverside-load',bt_serverside_load,name="bt_serverside"),

    path('jtable/list',JTable.as_view(),name="jtable"),
    path('jtable/reload4',reload4,name="reload4"),
    path('jtable/new',jtable_new,name="jtable_new"),
    path('jtable/edit',jtable_edit,name="jtable_edit"),
    path('jtable/delete',jtable_delete,name="jtable_delete"),

    path('dt/',HWHome.as_view(),name="hwhome"),
    path('dt/hw25000_reload',hw25000_reload,name="hw25000_reload"),

    path('dt/serverside',DTServerSide.as_view(),name="dt_serverside"),
    path('dt/serverside_reload',dt_serverside,name="dt_serverside_reload"),
]