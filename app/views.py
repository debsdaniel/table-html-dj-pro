import json
from django.db import reset_queries,Error
from django.http import JsonResponse
from django.shortcuts import render,HttpResponse
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger

from django.views.decorators.csrf import csrf_exempt

from .models import *
from .forms import *

class ModeloView(generic.ListView):
    permission_required = "app.view_modelo"
    model = Modelo
    template_name = "app/modelo_list.html"
    context_object_name = "obj"

class ModeloNew(generic.CreateView):
    permission_required="app.add_modelo"
    model=Modelo
    template_name="app/modelo_form.html"
    context_object_name = "obj"
    form_class=ModeloForm
    success_url=reverse_lazy("app:modelo_list")
    

class ModeloEdit(generic.UpdateView):
    permission_required="app.change_modelo"
    model=Modelo
    template_name="app/modelo_form.html"
    context_object_name = "obj"
    form_class=ModeloForm
    success_url=reverse_lazy("app:modelo_list")
    success_message="Modelo Actualizado Satisfactoriamente"

    def form_valid(self, form):
        form.instance.um = self.request.user.id
        return super().form_valid(form)

class ModeloDel(generic.DeleteView):
    permission_required="app.delete_modelo"
    model=Modelo
    template_name='app/catalogos_del.html'
    context_object_name='obj'
    success_url=reverse_lazy("app:modelo_list")
    success_message="Modelo Eliminado Satisfactoriamente"


class Crud(generic.TemplateView):
    template_name = "app/crud.html"


def update1(request):
    if request.POST:
        c = request.POST.get("codigo")
        d = request.POST.get("desc")

        print(c,d)
        o = Modelo.objects.filter(codigo=c).first()
        if o:
            o.descripcion = d
        else:
            o = Modelo(
                codigo = c,
                descripcion = d
            )
        o.save()

        return HttpResponse("OK")
    return HttpResponse("Método No Permitido")


def reload(request):
    context = {}
    registros = Modelo.objects.all()
    context["datos"] = list(registros.values("id","codigo","descripcion").order_by("codigo"))
    return JsonResponse(context,safe=False)


def update2(request):
    if request.method == "POST":
        datos = request.POST.getlist("datos[]")
        # print(datos)

        for i in datos:
            r = json.loads(i)
            print(r)
            c = r["codigo"]
            d = r["desc"]

            o = Modelo.objects.filter(codigo=c).first()
            if o:
                o.descripcion = d
            else:
                o = Modelo(
                    codigo = c,
                    descripcion = d
                )
            o.save()
        return HttpResponse("OK")
    return HttpResponse("Método NO Permitido")

class BootsTable(generic.TemplateView):
    template_name = "app/bootstable.html"


class DataTable(generic.TemplateView):
    template_name = "app/datatable.html"

def datatable(request):
    contexto = {}
    contexto["data"] = []


    if request.method == "POST":
        error = None
        r = request.POST
        print(r)
        # print(request.POST)
        print(r['action'])
        # print(list(r)[0])
        # print(r[list(r)[0]])
        # print(r[list(r)[1]])
        # print(r[list(r)[2]])
        
        # datos.append({"id":"1","codigo":"codigo","descripcion":"descripcion"})
        accion = r['action']
        if accion=='remove':
            id = r[list(r)[0]]

            o = Modelo.objects.filter(id=id)
            if o:
                o.delete()
            else:
                error = "Imposible Eliminar"
        else:
            cod = r[list(r)[0]]
            des   = r[list(r)[1]]
            print(cod,des)

            o = Modelo.objects.filter(codigo=cod).first()
            if not o :
                if accion=="create":
                    o = Modelo(
                        codigo = cod,
                        descripcion = des
                    )
                else:
                    error = "Código No Existe, imposible Editar"
            else:
                o.codigo = cod
                o.descripcion = des
            o.save()
            if not o:
                error = "No se ha podido guardar ni editar"
            id = o.id                    

            datos = []
            datos.append({
                "id":o.id,
                "codigo":o.codigo,
                "descripcion":o.descripcion
                })

            print(datos)
            contexto["data"] = datos
            if error:
                contexto["error"] = error
        print(contexto)
        
    return JsonResponse(contexto, safe=False)


class DataTableCustom(generic.TemplateView):
    template_name = "app/datatable_custom.html"


# class BoostrapTable(generic.ListView):
#     permission_required = "app.view_modelo"
#     model = Modelo
#     template_name = "app/bootstrap-table.html"
#     context_object_name = "obj"


class BoostrapTable(generic.TemplateView):
    template_name = "app/bootstrap-table.html"


def reload2(request):
    context = {}
    registros = Modelo.objects.all()
    context["total"] = registros.count()
    context["totalNotFiltered"] = registros.count()
    context["rows"] = list(registros.values("id","codigo","descripcion").order_by("codigo"))
    return JsonResponse(context,safe=False)


class JTable(generic.TemplateView):
    template_name = "app/jtable.html"


@csrf_exempt
def reload4(request):
    context = {}
    if request.method == "POST":
        registros = Modelo.objects.all()
        
        context["Records"] = list(registros.values("id","codigo","descripcion").order_by("codigo"))
        context["Result"] = "OK"
    else:
        context["Result"] = "ERROR"
        context["Message"] = "Método no permitido"

    return JsonResponse(context,safe=False)


def jtable_new(request):
    context = {}
    context["Result"] = "OK"
    context["Message"] = ""

    if request.method == "POST":
        c = request.POST.get("codigo")
        d = request.POST.get("descripcion")

        # print(c,d)

        if not c:
            context["Result"] = "ERROR"
            context["Message"] = "Código es Requerido"
            return JsonResponse(context,safe=False)        

        if not d:
            context["Result"] = "ERROR"
            context["Message"] = "Descripción es Requerida"
            return JsonResponse(context,safe=False)        

        o = Modelo(
            codigo = c,
            descripcion = d
        )
        try:
            o.save()
        except Error as e:
            context["Result"] = "ERROR"
            msg_error = str(e).split("DETAIL:")
            context["Message"] = msg_error[1]

        context["Record"] = {"id":o.id,"codigo":o.codigo,"descripcion":o.descripcion}
    
    return JsonResponse(context,safe=False)




def jtable_edit(request):
    context = {}
    context["Result"] = "OK"
    context["Message"] = ""

    if request.method == "POST":
        c = request.POST.get("codigo")
        d = request.POST.get("descripcion")
        i = request.POST.get("id")

        # print(c,d)

        if not c:
            context["Result"] = "ERROR"
            context["Message"] = "Código es Requerido"
            return JsonResponse(context,safe=False)        

        if not d:
            context["Result"] = "ERROR"
            context["Message"] = "Descripción es Requerida"
            return JsonResponse(context,safe=False)        

        o = Modelo.objects.filter(id=i).first()
        o.codigo = c
        o.descripcion = d

        try:
            o.save()
        except Error as e:
            context["Result"] = "ERROR"
            msg_error = str(e).split("DETAIL:")
            context["Message"] = msg_error[1]

        context["Record"] = {"id":o.id,"codigo":o.codigo,"descripcion":o.descripcion}
    
    return JsonResponse(context,safe=False)



def jtable_delete(request):
    context = {}
    context["Result"] = "OK"
    context["Message"] = ""

    if request.method == "POST":
        i = request.POST.get("id")

        # print(c,d)

        if not i:
            context["Result"] = "ERROR"
            context["Message"] = "No se recibió Id del Registro"
            return JsonResponse(context,safe=False)        

        o = Modelo.objects.filter(id=i).first()
        if not o:
            context["Result"] = "ERROR"
            context["Message"] = "Ningún registro coincide con el Id enviado"
            return JsonResponse(context,safe=False)        

        try:
            o.delete()
        except Error as e:
            context["Result"] = "ERROR"
            msg_error = str(e).split("DETAIL:")
            context["Message"] = msg_error[1]
    
    return JsonResponse(context,safe=False)


class HWHome(generic.TemplateView):
    template_name = "app/hw25000.html"


def hw25000_reload(request):
    context = {}
    registros = HW.objects.all()
    context["datos"] = list(registros.values("id","height","weight").order_by("id"))
    return JsonResponse(context,safe=False)


class DTServerSide(generic.TemplateView):
    template_name = "app/dtserverside.html"


def dt_serverside(request):
    context = {}

    dt = request.GET

    draw = int(dt.get("draw"))
    start = int(dt.get("start"))
    length = int(dt.get("length"))
    search = dt.get("search[value]")

    registros = HW.objects.all().order_by("id")

    if search:
        registros = registros.filter(
            Q(id__icontains=search) |
            Q(height__icontains=search) |
            Q(weight__icontains=search) 
        )

    recordsTotal = registros.count()
    recordsFiltered = recordsTotal

    # Preparamos la salida
    context["draw"] = draw
    context["recordsTotal"] = recordsTotal
    context["recordsFiltered"] = recordsFiltered

    reg = registros[start:start + length]
    paginator = Paginator(reg,length)

    try:
        obj = paginator.page(draw).object_list
    except PageNotAnInteger:
        obj = paginator.page(draw).object_list
    except EmptyPage:
        obj = paginator.page(paginator.num_pages).object_list

    datos = [
        {
           "id" : d.id,"height" : d.height, "weight" : d.weight
        } for d in obj
    ]
    context["datos"] = datos
    return JsonResponse(context,safe=False)
    
class BootstrapTableServerSide(generic.TemplateView):
    template_name = "app/bt-serverside.html"


def bt_serverside_load(request):
    context = {}
    registros = HW.objects.all()

    qs = request.GET
    offset = qs.get('offset')
    limit = qs.get('limit')
    search = qs.get('search')

    if offset:
        offset = int(offset)

    if limit:
        limit = int(limit)

    if search:
        registros = registros.filter(
            Q(id__icontains=search) |
            Q(height__icontains=search) |
            Q(weight__icontains=search) 
        )


    context["total"] = registros.count()
    context["totalNotFiltered"] = registros.count()
    context["rows"] = list(registros[offset:offset + limit].values("id","height","weight"))
    return JsonResponse(context,safe=False)